const user = require('./user')

test('should return Daniel', () => {
  expect(user.generateUser()).toBe('Daniel')
})